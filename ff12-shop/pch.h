#pragma once
#include <string>
#include <memory>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <regex>
#include <vector>
#include <limits>
#include <filesystem>
#include <io.h>
#include <fcntl.h>

#include "typedefs.h"
#include "ParseHelper.h"
#include "StringHelper.h"
#include "ShopContainer.h"
