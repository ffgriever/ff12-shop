#include "pch.h"
#include "ShopContainer.h"

namespace ff12::shop
{
	ShopContainerException::ShopContainerException(const std::wstring& msg)
		: std::runtime_error("")
		, _msg(msg)
	{
	}
	std::wstring ShopContainerException::what()
	{
		return _msg;
	}

	void ShopContainer::loadBinary(const std::wstring& inputFile)
	{
		namespace fs = std::filesystem;

		std::fstream binaryFile(inputFile, std::ios::in | std::ios::binary);
		if (!binaryFile)
			throw ShopContainerException(L"Cannot open binary file: " + inputFile);

		auto binarySize = fs::file_size(inputFile);
		if (binarySize < 12)
			throw ShopContainerException(L"Binary file " + inputFile + L" has invalid size");

		std::unique_ptr<u8> binaryData(new u8[static_cast<size_t>(binarySize)]);
		binaryFile.read(reinterpret_cast<char*>(binaryData.get()), binarySize);
		binaryFile.close();

		_loadBinaryShops(binaryData.get(), _shopDefinitions, 0, binarySize);
	}

	void ShopContainer::loadText(const std::wstring& inputFile)
	{
		std::fstream textFile(inputFile, std::ios::in);
		if (!textFile)
			throw ShopContainerException(L"Cannot open text file: " + inputFile);

		std::string contents;
		std::string line;
		while (std::getline(textFile, line))
		{
			static const std::regex commentRegex("//.*|\\s+");
			line = std::regex_replace(line, commentRegex, "");
			contents += line;
		}

		_importShops(contents, _shopDefinitions);
	}

	void ShopContainer::saveBinary(const std::wstring& outputFile)
	{
		std::fstream binaryFile(outputFile, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!binaryFile)
			throw ShopContainerException(L"Cannot save binary file: " + outputFile);

		uintmax_t currentPos = 0;
		//shop header
		ShopHeader shopHeader;
		shopHeader.shopsCount = static_cast<u16>(_shopDefinitions.size());
		binaryFile.write(reinterpret_cast<char*>(&shopHeader), sizeof(shopHeader));

		//shop definitions
		binaryFile.seekp(sizeof(ShopDefinitionEntry) * shopHeader.shopsCount, std::ios::cur);

		std::vector<ShopDefinitionEntry> shopDefinitionEntries;
		for (const auto& shopDefinition : _shopDefinitions)
		{
			auto& shopBack = shopDefinitionEntries.emplace_back();
			shopBack.shopName = shopDefinition.shopName;

			currentPos = binaryFile.tellp();
			shopBack.eventHeaderStart = static_cast<u32>(currentPos);
			//event header
			ShopEventHeader shopEventHeader;
			shopEventHeader.eventDefinitionStart = static_cast<u32>(currentPos + sizeof(shopEventHeader));
			shopEventHeader.eventCount = static_cast<u16>(shopDefinition.events.size());
			binaryFile.write(reinterpret_cast<char*>(&shopEventHeader), sizeof(shopEventHeader));
	
			//event definition
			uintmax_t currentItemStart = currentPos + sizeof(shopEventHeader) + shopDefinition.events.size() * sizeof(ShopEventDefinitionEntry);
			for (const auto& shopEvent : shopDefinition.events)
			{
				ShopEventDefinitionEntry shopEventDefinitionEntry{
					shopEvent.condition, shopEvent.address, shopEvent.conditionValue,
					static_cast<u32>(currentItemStart)
				};
				binaryFile.write(reinterpret_cast<char*>(&shopEventDefinitionEntry), sizeof(shopEventDefinitionEntry));
				currentItemStart += sizeof(ItemHeader) + (((shopEvent.items.size() + 1) / 2) * 2) * sizeof(shopEvent.items[0]);
			}

			//items
			for (const auto& shopEvent : shopDefinition.events)
			{
				if (!shopEvent.items.size())
					throw ShopContainerException(L"Even cannot have zero items!");

				currentPos = binaryFile.tellp();
				ItemHeader itemHeader;
				itemHeader.itemCount = static_cast<u16>(shopEvent.items.size());
				itemHeader.itemDefinitionStart = static_cast<u32>(currentPos + sizeof(itemHeader));
				binaryFile.write(reinterpret_cast<char*>(&itemHeader), sizeof(itemHeader));
				binaryFile.write(reinterpret_cast<const char*>(shopEvent.items.data()), sizeof(shopEvent.items[0]) * shopEvent.items.size());
				if (shopEvent.items.size() % 2)
				{
					u16 item = 0;
					binaryFile.write(reinterpret_cast<char*>(&item), sizeof(item));
				}
			}
		}

		binaryFile.seekp(sizeof(shopHeader), std::ios::beg);
		binaryFile.write(reinterpret_cast<char*>(shopDefinitionEntries.data()), shopDefinitionEntries.size() * sizeof(shopDefinitionEntries[0]));

	}

	void ShopContainer::saveText(const std::wstring& outputFile)
	{
		std::stringstream content;

		content << "{format:shop}" << std::endl << std::endl;

		content << _exportShops(_shopDefinitions, 0);

		std::fstream textFile(outputFile, std::ios::out | std::ios::trunc);
		if (!textFile)
			throw ShopContainerException(L"Cannot save text file: " + outputFile);

		textFile.write(content.str().c_str(), content.str().size());
	}

	void ShopContainer::_loadBinaryShops(const u8* data, std::vector<ShopDefinition>& shops, u32 headerStart, uintmax_t binarySize)
	{
		shops.clear();
		const ShopHeader* shopHeader = reinterpret_cast<const ShopHeader*>(data);
		if (shopHeader->magic != CONTAINER_MAGIC || shopHeader->entryLength != sizeof(ShopDefinitionEntry) || shopHeader->shopsStart != sizeof(ShopHeader))
			throw ShopContainerException(L"Unsupported binary file header");

		if (shopHeader->entryLength * shopHeader->shopsCount + shopHeader->shopsStart > binarySize)
			throw ShopContainerException(L"Binary file too small");

		const ShopDefinitionEntry* shopDefinitionEntries = reinterpret_cast<const ShopDefinitionEntry*>(data + shopHeader->shopsStart);

		for (auto shopNum = 0; shopNum < shopHeader->shopsCount; shopNum++)
		{
			auto& shopBack = _shopDefinitions.emplace_back();
			shopBack.shopName = shopDefinitionEntries[shopNum].shopName;

			_loadBinaryEvents(data, shopBack.events, shopDefinitionEntries[shopNum].eventHeaderStart, binarySize);
		}
	}

	void ShopContainer::_loadBinaryEvents(const u8* data, std::vector<ShopEvent>& events, u32 headerStart, uintmax_t binarySize)
	{
		const ShopEventHeader* eventHeader = reinterpret_cast<const ShopEventHeader*>(data + headerStart);
		if (eventHeader->magic != EVENT_MAGIC || eventHeader->entryLength != sizeof(ShopEventDefinitionEntry))
			throw ShopContainerException(L"Unsupported event file header");

		if (eventHeader->entryLength * eventHeader->eventCount + eventHeader->eventDefinitionStart > binarySize)
			throw ShopContainerException(L"Binary file too small for all events");

		const ShopEventDefinitionEntry* shopEventDefinitionEntries = reinterpret_cast<const ShopEventDefinitionEntry*>(data + eventHeader->eventDefinitionStart);
		for (auto eventNum = 0; eventNum < eventHeader->eventCount; eventNum++)
		{
			auto& eventsBack = events.emplace_back();
			eventsBack.condition = shopEventDefinitionEntries[eventNum].condition;
			eventsBack.address = shopEventDefinitionEntries[eventNum].address;
			eventsBack.conditionValue = shopEventDefinitionEntries[eventNum].conditionValue;

			_loadBinaryItems(data, eventsBack.items, shopEventDefinitionEntries[eventNum].itemHeaderStart, binarySize);
		}
	}

	void ShopContainer::_loadBinaryItems(const u8* data, std::vector<u16>& items, u32 headerStart, uintmax_t binarySize)
	{
		const ItemHeader* itemHeader = reinterpret_cast<const ItemHeader*>(data + headerStart);
		if (itemHeader->magic != ITEM_MAGIC || itemHeader->entryLength != sizeof(u16))
			throw ShopContainerException(L"Unsupported item file header");

		if (itemHeader->entryLength * itemHeader->itemCount + itemHeader->itemDefinitionStart > binarySize)
			throw ShopContainerException(L"Binary file too small for all items");

		const u16* itemDefinitions = reinterpret_cast<const u16*>(data + itemHeader->itemDefinitionStart);
		for (auto itemNum = 0; itemNum < itemHeader->itemCount; itemNum++)
		{
			items.push_back(itemDefinitions[itemNum]);
		}
	}

	std::string ShopContainer::_exportShops(const std::vector<ShopDefinition>& shops, int indent)
	{
		std::stringstream content;
		size_t shopNum = 0;
		for (const auto& shop : shops)
		{
			content << StringHelper::indent(indent) << "shop" << shopNum << " { //" << StringHelper::printActorName(shop.shopName) << std::endl;
			indent++;

			content << StringHelper::exportValue("shopName", shop.shopName, indent, false);
			content << _exportEvents(shop.events, indent);

			indent--;
			content << StringHelper::indent(indent) << "};" << std::endl;
			shopNum++;
		}
		return content.str();
	}

	std::string ShopContainer::_exportEvents(const std::vector<ShopEvent>& events, int indent)
	{
		std::stringstream content;
		size_t eventNum = 0;
		for (const auto& event : events)
		{
			content << StringHelper::indent(indent) << "event" << eventNum << " {" << std::endl;
			indent++;

			if (event.condition > 0) content << StringHelper::exportValue("condition", event.condition, indent, false);
			if (event.address > 0) content << StringHelper::exportValue("address", event.address, indent, false);
			if (event.conditionValue > 0) content << StringHelper::exportValue("conditionValue", event.conditionValue, indent);
			content << StringHelper::exportArrayWrap("itemList", event.items.data(), event.items.size(), indent);

			indent--;
			content << StringHelper::indent(indent) << "};" << std::endl;
			eventNum++;
		}
		return content.str();
	}

	void ShopContainer::_importShops(const std::string& shopText, std::vector<ShopDefinition>& shopEntries)
	{
		shopEntries.clear();
		std::vector<std::string> shops;
		ParseHelper::getSectionMultiple(shopText, "shop", shops);

		for (const auto& shop : shops)
		{
			auto& shopBack = _shopDefinitions.emplace_back();
			ParseHelper::getSingleVal(shop, "shopName", shopBack.shopName);
			_importEvents(shop, shopBack.events);
		}
	}

	void ShopContainer::_importEvents(const std::string& eventText, std::vector<ShopEvent>& eventEntries)
	{
		eventEntries.clear();
		std::vector<std::string> events;
		ParseHelper::getSectionMultiple(eventText, "event", events);
		for (const auto& event : events)
		{
			auto& eventBack = eventEntries.emplace_back();
			ParseHelper::getSingleVal(event, "condition", eventBack.condition);
			ParseHelper::getSingleVal(event, "address", eventBack.address);
			ParseHelper::getSingleVal(event, "conditionValue", eventBack.conditionValue);
			ParseHelper::getSingleArray(event, "itemList", eventBack.items);
		}
	}

}
