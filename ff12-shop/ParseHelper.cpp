#include "pch.h"
#include "ParseHelper.h"

namespace ff12
{
	void ParseHelper::getSectionMultiple(const std::string& source, const std::string& sectionName, std::vector<std::string>& matches)
	{
		//circumventing limitations and stack overflows
		const std::regex searchRegex(sectionName + "[0-9]+\\{(.*?)\\};(" + sectionName + "|$)");
		std::smatch searchMatch;
		auto it = source.cbegin();
		while (std::regex_search(it, source.cend(), searchMatch, searchRegex))
		{
			matches.push_back(searchMatch[1]);
			it = searchMatch[2].first;
		}
	}
}
