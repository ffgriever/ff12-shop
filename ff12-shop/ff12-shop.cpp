#include "pch.h"
#include <Windows.h>
#include <Shlwapi.h>
#include "version.h"

void printBanner()
{
	std::wcout << "FF12 shop tool v" VERSION_FULL " by ffgriever" << std::endl << std::endl;
}

void printUsage(const std::wstring& message = L"")
{
	printBanner();
	if (message.size()) std::wcout << message << std::endl << std::endl;
	std::wcout << "Usage: ff12-shop option input output" << std::endl << std::endl;
	std::wcout << "Options:" << std::endl;
	std::wcout << "	-u		unpacks binary file into text" << std::endl;
	std::wcout << "			(output defaults to {input}.txt)" << std::endl;
	std::wcout << "	-p		packs text into binary file" << std::endl;
	std::wcout << "			(output defaults to {input}.bin)" << std::endl << std::endl;
}

enum class operatingModes
{
	enModeUnknown,
	enModePack,
	enModeUnpack,
};

int wmain(int argc, wchar_t** argv)
{
	using namespace ff12;
	try
	{
		int previousMode = _setmode(_fileno(stdout), _O_U16TEXT);
		int previousErrorMode = _setmode(_fileno(stderr), _O_U16TEXT);

		operatingModes mode = operatingModes::enModeUnknown;
		std::wstring inputFile;
		std::wstring outputFile;

		if (argc < 3)
		{
			printUsage();
			return EXIT_FAILURE;
		}

		for (int i = 1; i < argc; i++)
		{
			std::wstring arg(argv[i]);
			if (arg[0] == L'-')
			{
				if (mode != operatingModes::enModeUnknown)
				{
					printUsage(L"Too many options specified");
					return EXIT_FAILURE;
				}
				if (arg == L"-u") mode = operatingModes::enModeUnpack;
				else if (arg == L"-p") mode = operatingModes::enModePack;
				else { printUsage(L"Invalid option"); return EXIT_FAILURE; }
			}
			else
			{
				if (inputFile.size() == 0)
				{
					inputFile = argv[i];
				}
				else if (outputFile.size() == 0)
				{
					outputFile = argv[i];
				}
				else
				{
					printUsage(L"Too many files specified");
					return EXIT_FAILURE;
				}
			}
		}
		if (!inputFile.size() || mode == operatingModes::enModeUnknown)
		{
			printUsage(L"Invalid parameters");
			return EXIT_FAILURE;
		}

		if (!outputFile.size() && mode == operatingModes::enModePack) outputFile = inputFile + L".bin";
		else if (!outputFile.size() && mode == operatingModes::enModeUnpack) outputFile = inputFile + L".txt";


		if (outputFile.size() == 0)
			throw std::runtime_error("No output file specified");

		if (mode == operatingModes::enModeUnpack)
		{
			shop::ShopContainer shop;
			shop.loadBinary(inputFile);
			shop.saveText(outputFile);
			std::wcout << "Done!" << std::endl;
		}
		else if (mode == operatingModes::enModePack)
		{
			shop::ShopContainer shop;
			shop.loadText(inputFile);
			shop.saveBinary(outputFile);
			std::wcout << "Done!" << std::endl;
		}

		return EXIT_SUCCESS;
	}
	catch (shop::ShopContainerException& e)
	{
		std::wcerr << L"Error: " << e.what() << std::endl;
	}
	catch (std::runtime_error& e)
	{
		std::wcerr << L"Error: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch (...)
	{
		std::wcerr << L"Unknown error in main" << std::endl;
		return EXIT_FAILURE;
	}
}
