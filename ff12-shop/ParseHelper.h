#pragma once


namespace ff12
{

	class ParseHelper
	{
	public:
		//expects no linebreaks and no spaces
		static void getSectionMultiple(const std::string& source, const std::string& sectionName, std::vector<std::string>& matches);

		template<typename IntegerType>
		static void getSingleVal(const std::string& source, const std::string& valueName, IntegerType& retval, IntegerType defVal = 0)
		{
			const std::regex searchRegex(valueName + "=(0x[0-9a-fA-F]+?|[0-9]+?);");
			std::smatch searchMatch;
			if (std::regex_search(source, searchMatch, searchRegex))
			{
				if (std::is_unsigned<IntegerType>::value)
				{
					uint64_t val = std::stoull(searchMatch[1], nullptr, 0);
					if (val > std::numeric_limits<IntegerType>::max())
						throw std::overflow_error(std::string("Value of " + valueName + " out of range for this variable type").c_str());
					retval = static_cast<IntegerType>(val);
				}
				else
				{
					int64_t val = std::stoll(searchMatch[1], nullptr, 0);
					if (val > std::numeric_limits<IntegerType>::max() || val < std::numeric_limits<IntegerType>::min())
						throw std::overflow_error(std::string("Value of " + valueName + " out of range for this variable type").c_str());
					retval = static_cast<IntegerType>(val);
				}
			}
			else
			{
				retval = defVal;
			}
		}

		static void getSingleVal(const std::string& source, const std::string& valueName, float& retval, float defVal = 0.0f)
		{
			const std::regex searchRegex(valueName + "=((\\+|-)?[0-9]+(\\.[0-9]+)?((e|E)(\\+|-)?[0-9]+)?);");
			std::smatch searchMatch;
			if (std::regex_search(source, searchMatch, searchRegex))
			{
				retval = std::stof(searchMatch[1]);
			}
			else
			{
				retval = defVal;
			}
		}

		template<typename IntegerType>
		static void getSingleArray(const std::string& source, const std::string& valueName, IntegerType* retval, size_t size)
		{
			const std::regex searchRegex(valueName + "\\[\\]=\\{((0x[0-9a-fA-F]+?|[0-9]+?)(,(0x[0-9a-fA-F]+?|[0-9]+?))*?)\\};");
			std::smatch searchMatch;
			if (std::regex_search(source, searchMatch, searchRegex))
			{
				std::string arrString = searchMatch[1];
				static const std::regex arrRegex("(0x[0-9a-fA-F]+|[0-9]+)");
				std::smatch arrMatch;
				auto it = arrString.cbegin();
				size_t i = 0;
				while (std::regex_search(it, arrString.cend(), arrMatch, arrRegex))
				{
					if (i >= size)
						throw std::out_of_range(std::string("Subscript out of range while parsing " + valueName + " array").c_str());

					if (std::is_unsigned<IntegerType>::value)
					{
						uint64_t val = std::stoull(arrMatch[1], nullptr, 0);
						if (val > std::numeric_limits<IntegerType>::max())
							throw std::overflow_error(std::string("Value of " + valueName + " out of range for this variable type").c_str());
						retval[i] = static_cast<IntegerType>(val);
					}
					else
					{
						int64_t val = std::stoll(arrMatch[1], nullptr, 0);
						if (val > std::numeric_limits<IntegerType>::max() || val < std::numeric_limits<IntegerType>::min())
							throw std::overflow_error(std::string("Value of " + valueName + " out of range for this variable type").c_str());
						retval[i] = static_cast<IntegerType>(val);
					}
					it = arrMatch[1].second;
					i++;
				}
			}
		}

		template<typename IntegerType>
		static void getSingleArray(const std::string& source, const std::string& valueName, std::vector<IntegerType>& retval)
		{
			retval.clear();
			const std::regex searchRegex(valueName + "\\[\\]=\\{(.+?)\\};");
			std::smatch searchMatch;
			if (std::regex_search(source, searchMatch, searchRegex))
			{
				std::string arrString = searchMatch[1];
				static const std::regex arrRegex("(0x[0-9a-fA-F]+|[0-9]+)");
				std::smatch arrMatch;
				auto it = arrString.cbegin();
				while (std::regex_search(it, arrString.cend(), arrMatch, arrRegex))
				{

					if (std::is_unsigned<IntegerType>::value)
					{
						uint64_t val = std::stoull(arrMatch[1], nullptr, 0);
						if (val > std::numeric_limits<IntegerType>::max())
							throw std::overflow_error(std::string("Value of " + valueName + " out of range for this variable type").c_str());
						retval.push_back(static_cast<IntegerType>(val));
					}
					else
					{
						int64_t val = std::stoll(arrMatch[1], nullptr, 0);
						if (val > std::numeric_limits<IntegerType>::max() || val < std::numeric_limits<IntegerType>::min())
							throw std::overflow_error(std::string("Value of " + valueName + " out of range for this variable type").c_str());
						retval.push_back(static_cast<IntegerType>(val));
					}
					it = arrMatch[1].second;
				}
			}
		}

		static void getSingleArray(const std::string& source, const std::string& valueName, float* retval, size_t size)
		{
			const std::regex searchRegex(valueName + "\\[\\]=\\{(((\\+|-)?[0-9]+(\\.[0-9]+)?((e|E)(\\+|-)?[0-9]+)?)(,((\\+|-)?[0-9]+(\\.[0-9]+)?((e|E)(\\+|-)?[0-9]+)?))*?)\\};");
			std::smatch searchMatch;
			if (std::regex_search(source, searchMatch, searchRegex))
			{
				std::string arrString = searchMatch[1];
				static const std::regex arrRegex("((\\+|-)?[0-9]+(\\.[0-9]+)?((e|E)(\\+|-)?[0-9]+)?)");
				std::smatch arrMatch;
				auto it = arrString.cbegin();
				size_t i = 0;
				while (std::regex_search(it, arrString.cend(), arrMatch, arrRegex))
				{
					if (i >= size)
						throw std::out_of_range(std::string("Subscript out of range while parsing " + valueName + " array").c_str());

					retval[i] = stof(arrMatch[1]);
					it = arrMatch[1].second;
					i++;
				}
			}
		}

		template<typename IntegerType>
		static void getSingleArrayDirect(const std::string& source, const std::string& valueName, IntegerType* retval, size_t size)
		{
			static const std::regex arrRegex("(0x[0-9a-fA-F]+|[0-9]+)");
			std::smatch arrMatch;
			auto it = source.cbegin();
			size_t i = 0;
			while (std::regex_search(it, source.cend(), arrMatch, arrRegex))
			{
				if (i >= size)
					throw std::out_of_range(std::string("Subscript out of range while parsing " + valueName + " array").c_str());

				if (std::is_unsigned<IntegerType>::value)
				{
					uint64_t val = std::stoull(arrMatch[1], nullptr, 0);
					if (val > std::numeric_limits<IntegerType>::max())
						throw std::overflow_error(std::string("Value of " + valueName + " out of range for this variable type").c_str());
					retval[i] = static_cast<IntegerType>(val);
				}
				else
				{
					int64_t val = std::stoll(arrMatch[1], nullptr, 0);
					if (val > std::numeric_limits<IntegerType>::max() || val < std::numeric_limits<IntegerType>::min())
						throw std::overflow_error(std::string("Value of " + valueName + " out of range for this variable type").c_str());
					retval[i] = static_cast<IntegerType>(val);
				}
				it = arrMatch[1].second;
				i++;
			}
		}
	};

}
