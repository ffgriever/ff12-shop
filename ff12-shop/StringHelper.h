#pragma once

namespace ff12
{


	class StringHelper
	{
	public:
		static std::string indent(int indentation, size_t numberOfRepeats, char indentCharacter)
		{
			return std::string(numberOfRepeats * indentation, indentCharacter);
		}

		static std::string indent(int indentation)
		{
			return indent(indentation, 1, '\t');
		}

		template<typename IntegerType>
		static std::string printArray(const IntegerType items[], size_t size, bool hex = true)
		{
			std::stringstream ret;
			bool first = true;
			if (hex) ret << std::hex;
			for (size_t i = 0; i < size; i++)
			{
				if (!first)
					ret << ", ";
				else
					first = false;
				if (hex) ret << "0x";
				
				ret << +items[i];
			}
			return ret.str();
		}

		template<typename IntegerType>
		static std::string printArrayWrap(const IntegerType items[], size_t size, int indentation, size_t wrap = 10, bool hex = true)
		{
			std::stringstream ret;
			bool first = true;
			if (hex) ret << std::hex;
			indentation++;

			for (size_t i = 0; i < size; i++)
			{
				if (!first)
					ret << ", ";
				else
					first = false;
				if (size > wrap && i % wrap == 0) ret << std::endl << indent(indentation);
				if (hex) ret << "0x";

				ret << +items[i];
			}
			indentation--;
			if (size > wrap) ret << std::endl << indent(indentation);
			return ret.str();
		}
		
		static std::string printArray(const float items[], size_t size, bool hex = true)
		{
			std::stringstream ret;
			bool first = true;
			ret << std::setprecision(std::numeric_limits<float>::max_digits10);
				
			for (size_t i = 0; i < size; i++)
			{
				if (!first)
					ret << ", ";
				else
					first = false;

				ret << items[i];
			}
			return ret.str();
		}

		template<typename IntegerType>
		static std::string exportValue(const std::string& valueName, IntegerType value, int indentation, bool hex = true)
		{
			std::stringstream ret;
			ret << indent(indentation) << valueName << " = ";
			if (hex)
				ret << "0x" << std::hex;
			ret << +value << ";" << std::endl;
			return ret.str();
		}

		static std::string exportValue(const std::string& valueName, float value, int indentation, bool hex = true)
		{
			std::stringstream ret;
			ret << std::setprecision(std::numeric_limits<float>::max_digits10);
			ret << indent(indentation) << valueName << " = ";
			ret << value << ";" << std::endl;
			return ret.str();
		}

		template<typename T>
		static std::string exportArray(const std::string& arrayName, const T items[], size_t size, int indentation, bool hex = true)
		{
			std::stringstream ret;
			ret << indent(indentation) << arrayName << "[] = {" << printArray(items, size, hex) << "};" << std::endl;
			return ret.str();
		}

		template<typename T>
		static std::string exportArrayWrap(const std::string& arrayName, const T items[], size_t size, int indentation, size_t wrap = 10, bool hex = true)
		{
			std::stringstream ret;
			ret << indent(indentation) << arrayName << "[] = {" << printArrayWrap(items, size, indentation, wrap, hex) << "};" << std::endl;
			return ret.str();
		}

		static std::string printActorName(uint32_t actorId, bool unraveled = true)
		{
			if (actorId >= 1140)
				return "UNKNOWN";

			return _names[actorId * 2 + (unraveled ? 1 : 0)];
		}

	private:
		static const char* _names[];
	};

}
