#pragma once

#define VERSION_MAJOR "1"
#define VERSION_MINOR "0"
#define VERSION_BUILD "a"

#define VERSION_FULL VERSION_MAJOR "." VERSION_MINOR VERSION_BUILD