#pragma once

namespace ff12::shop
{
	class ShopContainerException : public std::runtime_error
	{
	public:
		ShopContainerException(const std::wstring& msg = L"");
		std::wstring what();
		virtual ~ShopContainerException() = default;
	private:
		ShopContainerException() = delete;
		std::wstring _msg;
	};

	class ShopContainer
	{
	public:
		void loadBinary(const std::wstring& inputFile);
		void loadText(const std::wstring& inputFile);
		void saveBinary(const std::wstring& outputFile);
		void saveText(const std::wstring& outputFile);
	private:
		static constexpr u32 CONTAINER_MAGIC = 0x64;
		struct ShopHeader
		{
			u32 magic = CONTAINER_MAGIC;
			u16 entryLength = sizeof(struct ShopDefinitionEntry);
			u16 shopsCount = 0;
			u32 shopsStart = sizeof(ShopHeader);
		};

		struct ShopDefinitionEntry
		{
			u32 shopName = 0;
			u32 eventHeaderStart = 0;
		};

		static constexpr u32 EVENT_MAGIC = 0x164;
		struct ShopEventHeader
		{
			u32 magic = EVENT_MAGIC;
			u16 entryLength = sizeof(struct ShopEventDefinitionEntry);
			u16 eventCount = 0;
			u32 eventDefinitionStart = 0;
		};
		struct ShopEventDefinitionEntry
		{
			u8 condition; //0 - no condition, 1 - memory, 2 - scenario, 3  - clan rank
			u8 address; //used only with condition 1
			u16 conditionValue;
			u32 itemHeaderStart;
		};

		static constexpr u32 ITEM_MAGIC = 0x264;
		struct ItemHeader
		{
			u32 magic = ITEM_MAGIC;
			u16 entryLength = sizeof(u16);
			u16 itemCount = 0;
			u32 itemDefinitionStart = 0;
		};

		struct ShopEvent
		{
			u8 condition; //0 - no condition, 1 - memory, 2 - scenario, 3  - clan rank
			u8 address; //used only with condition 1
			u16 conditionValue;
			std::vector<u16> items; //always rounded up to 2
		};
		struct ShopDefinition
		{
			u32 shopName = 0;
			std::vector<ShopEvent> events;
		};

		void _loadBinaryShops(const u8* data, std::vector<ShopDefinition>& shops, u32 headerStart, uintmax_t binarySize);
		void _loadBinaryEvents(const u8* data, std::vector<ShopEvent>& events, u32 headerStart, uintmax_t binarySize);
		void _loadBinaryItems(const u8* data, std::vector<u16>& items, u32 headerStart, uintmax_t binarySize);

		std::string _exportShops(const std::vector<ShopDefinition>& shops, int indent);
		std::string _exportEvents(const std::vector<ShopEvent>& events, int indent);

		void _importShops(const std::string& shopText, std::vector<ShopDefinition>& shopEntries);
		void _importEvents(const std::string& eventText, std::vector<ShopEvent>& eventEntries);

		std::vector<ShopDefinition> _shopDefinitions;
	};

}
